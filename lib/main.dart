import 'package:flutter/material.dart';

void main() {
  runApp(const MyFirstApp());
}

class MyFirstApp extends StatelessWidget {
  const MyFirstApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: "psu trang",
      theme: ThemeData(
        primarySwatch: Colors.deepPurple,
      ),
      home: Scaffold(
        appBar: AppBar(
          leading: Icon(Icons.insert_invitation),
          title: Text('My fucking app'),
          actions: [IconButton(onPressed: () {}, icon: Icon(Icons.add_alarm))],
        ),
        body: Column(
          children: [
            Text(
              'นายนราวิชญ์ กิจวงศ์วัฒนา',
              style: TextStyle(height: 2, fontSize: 30),
            ),
            Text(
              'รหัสนศ. 6350110010',
              style: TextStyle(height: 1, fontSize: 20),
            ),
            Image.asset('asset/1.jpg'),
          ],
        ),
      ),
    );
  }
}

// class MyFirstApp extends StatelessWidget {
//   const MyFirstApp({Key? key}) : super(key: key);
//
//   @override
//   Widget build(BuildContext context) {
//     return MaterialApp(
//       title: 'Flutter Demo',
//       theme: ThemeData(
//         primarySwatch: Colors.blue,
//       ),
//       home:
//     );
//   }
// }
